#!/usr/bin/python3
# Copyright 2020 Fluid Numerics LLC.

import time
import shlex
import subprocess

COMPILERS = ['gnu']

MPI_FLAVORS = ['mvapich2',
               'openmpi']

OHPC_SERIAL_PKGS = ['hdf5',
                    'metis',
                    'openblas',
                    'ocr',
                    'pdtoolkit',
                    'python-numpy']

OHPC_MPI_PKGS = ['hypre',
                 'netcdf-cxx',
                 'netcdf-fortran',
                 'netcdf',
                 'petsc',
                 'phdf5',
                 'python-scipy',
                 'scalapack',
                 'scalasca',
                 'scorep',
                 'tau']

def install_ohpc(compilers, mpi_flavors, ohpc_serial_pkgs, ohpc_mpi_pkgs):
    '''Main routine for installing OpenHPC packages on CentOS systems'''

    subprocess.check_call(shlex.split('wget https://github.com/openhpc/ohpc/releases/download/v1.3.GA/ohpc-release-1.3-1.el7.x86_64.rpm'))
    subprocess.check_call(shlex.split('rpm -i ohpc-release-1.3-1.el7.x86_64.rpm'))
    subprocess.check_call(shlex.split('yum update -y'))
    subprocess.check_call(shlex.split('yum remove -y environment-modules'))

    base_packages = ['autoconf-ohpc',
                     'automake-ohpc',
                     'lmod-ohpc',
                     'docs-ohpc',
                     'papi-ohpc',
                     'valgrind-ohpc']


    ohpc_compilers = []
    ohpc_mpi = []
    inst = []
    mpi_inst = []

    for compiler in compilers:
        # Append compilers to list
        ohpc_compilers.append(compiler +'-compilers-ohpc')

        # Append MPI flavor to list
        this_mpi = [pkg + '-' + compiler +'-ohpc' for pkg in mpi_flavors]
        ohpc_mpi.extend(this_mpi)

        # Append serial packages to list
        this_inst = [pkg + '-' + compiler +'-ohpc' for pkg in ohpc_serial_pkgs]
        inst.extend(this_inst)

        # Append Compiler+MPI packages to list
        for mpi in mpi_flavors:
            this_mpi_inst = [pkg + '-' + compiler + '-' + mpi + '-ohpc' for pkg in ohpc_mpi_pkgs]
            mpi_inst.extend(this_mpi_inst)

    while subprocess.check_call(['yum', 'install', '-y'] + ohpc_compilers):
        print "yum failed to install packages. Trying again in 5 seconds"
        time.sleep(5)

    while subprocess.check_call(['yum', 'install', '-y'] + ohpc_mpi):
        print "yum failed to install packages. Trying again in 5 seconds"
        time.sleep(5)

    while subprocess.check_call(['yum', 'install', '-y'] + base_packages):
        print "yum failed to install packages. Trying again in 5 seconds"
        time.sleep(5)

    while subprocess.check_call(['yum', 'install', '-y'] + inst):
        print "yum failed to install packages. Trying again in 5 seconds"
        time.sleep(5)

    while subprocess.check_call(['yum', 'install', '-y'] + mpi_inst):
        print "yum failed to install packages. Trying again in 5 seconds"
        time.sleep(5)

    # Wait for logs to propagate
    time.sleep(15)


def main():
    '''Main routine that is called if running standalone'''

    install_ohpc(COMPILERS,
                 MPI_FLAVORS,
                 OHPC_SERIAL_PKGS,
                 OHPC_MPI_PKGS)

# END main()

if __name__ == '__main__':

    main()
