# README #
Copyright 2020 Fluid Numerics LLC.
Licensed to anyone who downloads this repository under the [Apache 2.0 License](./LICENSE)

fluid-slurm-gcp-ohpc is a simple python script for installing OpenHPC packages on platforms with the CentOS operating system.
At it's inception, this repository is intended for use with [Fluid Numerics' fluid-slurm-gcp Google Cloud marketplace solution](https://console.cloud.google.com/marketplace/details/fluid-cluster-ops/fluid-slurm-gcp?utm_source=bitbucket&utm_medium=repo&utm_campaign=feb_mktplc&utm_content=multiproject_ohpc)

### How do I get set up? ###

* [Review the available OHPC rpms](http://build.openhpc.community/OpenHPC:/1.3/CentOS_7/x86_64/)
* [Create a fluid-slurm-gcp cluster](https://console.cloud.google.com/marketplace/details/fluid-cluster-ops/fluid-slurm-gcp?utm_source=bitbucket&utm_medium=repo&utm_campaign=feb_mktplc&utm_content=multiproject_ohpc)
* Clone this repository on your cluster's controller node.
* Edit the `COMPILERS`, `MPI_FLAVORS`, `OHPC_SERIAL_PKGS`, and `OHPC_MPI_PKGS` near the top of `install_ohpc.py`. 
* Run `sudo python3 ./install_ohpc.py` and wait for installs to complete.

### Feedback & Issue Collection ###
[Submit your feedback](https://forms.gle/f3dsxRs98t49Qyqs6)
